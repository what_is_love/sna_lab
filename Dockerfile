FROM python:3.8
WORKDIR /
COPY . .
CMD ["python3", "-m", "http.server"]